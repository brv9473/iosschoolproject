//
//  SearchViewController.swift
//  TestForIOSSchoolProject
//
//  Created by Rostyslav Bodnar on 14.10.2021.
//

import UIKit

class SearchViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var questionTextField: UITextField!
    
    // MARK: - Override Properties
    override var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.becomeFirstResponder()
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            if questionTextField.text == "" {
                self.showNoQuestionAlert()
            }
            print("Your question - \(questionTextField.text ?? "N")")
        }
    }
}
