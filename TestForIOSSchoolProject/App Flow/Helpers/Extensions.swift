//
//  Extensions.swift
//  TestForIOSSchoolProject
//
//  Created by Rostyslav Bodnar on 14.10.2021.
//

import UIKit

extension UIViewController {
    func showNoQuestionAlert() {
        let errorMessage = "No question"
        
        let alertVC = UIAlertController(title: "Oops",
                                        message: errorMessage,
                                        preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertVC.addAction(okAction)
        present(alertVC, animated: true, completion: nil)
    }
    
    func showApiErrorAlert(with error: Error?) {
        let errorMessage =
            (error as? ErrorResponseModel)?.localizedDescription ?? error?.localizedDescription ?? "No description"
        
        let alertVC = UIAlertController(title: "Oops",
                                        message: errorMessage,
                                        preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertVC.addAction(okAction)
        present(alertVC, animated: true, completion: nil)
    }
}
