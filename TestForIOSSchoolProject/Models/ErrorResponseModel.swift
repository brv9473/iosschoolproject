//
//  ErrorResponseModel.swift
//  TestForIOSSchoolProject
//
//  Created by Rostyslav Bodnar on 14.10.2021.
//

import Foundation

struct ErrorResponseModel: Decodable, Error {
    let error: String
    let code: Int
    
    var localizedDescription: String {
        error
    }
}
